package com.junehansen;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class SearchWord {

    //Check for correct number of arguments
    public static void main(String[] args) {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: java SearchWord <folderName> <fileExtension> <keyword1> <keyword2> <keyword3>");
            return;
        }

        //Checks that first argument is an existing path
        File directory = new File(args[0]);
        if (!directory.isDirectory()) {
            System.out.println("The specified folder does not exist.");
            return;
        }


        //Check that second argument is a fileExtension
        if (!args[1].equals(".txt") && !args[1].equals(".docx")) {
            System.out.println("The file extension " + args[1] + " is not supported.");
            return;
        }

        //Get all files with specified extension
        File[] files = directory.listFiles((dir, name) -> name.toLowerCase().endsWith(args[1]));

        assert files != null;
        if (files.length < 1) {
            System.out.println("There are no files with that extension in that folder.");
            return;
        }

        //Create a map of total count for each keyword
        Map<String, Integer> counters = new HashMap<>();
        for (int i = 2; i < args.length; i++) {
            counters.put(args[i], 0);
        }

        //Create a map of occurrences for each file
        Map<String, Integer> occurrences = new HashMap<>();
        for (File file : files) {
            for (int i = 2; i < args.length; i++) {
                occurrences.put(file.getName() + "|" + args[i], 0);
            }
        }



        //TODO: Fix docx reading
        //Go through each file
        for (File file : files) {

            String fileString = null;

            if (file.getPath().endsWith(".txt")) {
                fileString = txtReader(file);
            } else {
                fileString = docReader(file);
            }

            if (fileString == null) continue;

            String[] words = fileString.split("\\W+");

            //Go through all keys and
            for (String key : counters.keySet()) {
                //Do while more words in the file
                for (String currentWord : words) {

                    //Get next word, convert to lowercase
                    currentWord = currentWord.toLowerCase();
                    //currentWord = currentWord.replaceAll("[^a-z0-9]", "");

                    //Check word against argument (in lowercase), increase counter if equal
                    if (currentWord.equals(key)) {
                        int totalCount = counters.get(key);
                        counters.put(key, ++totalCount);
                        int currentCount = occurrences.get(file.getName() + "|" + key);
                        occurrences.put(file.getName() + "|" + key, ++currentCount);
                    }
                }

            }
        }

        //Get # files in directory
        int dirCount = 0;
        dirCount = Objects.requireNonNull(directory.list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(args[1]);
            }
        })).length;

        //Build string to put in file
        StringBuilder printText = new StringBuilder("The folder \"" + args[0] + "\" contains " + dirCount + " number of " + args[1] + " files.");
        for (int i = 2; i < args.length; i++) {
            printText.append("\n\nThe keyword \"" + args[i] + "\" occurred " + counters.get(args[i]) + " times");

            for (File file : files) {
                printText.append("\n\t" + occurrences.get(file.getName() + "|" + args[i]) + " times in " + file.getName());
            }
        }

        System.out.println("Creating output.txt with results.");

        //Create output file
        try {
            File file = new File("output.txt");
            FileOutputStream fos = new FileOutputStream(file);

            //Create byte array to write to file
            byte[] bytesArray = printText.toString().getBytes();

            fos.write(bytesArray);
            fos.flush();

            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String txtReader(File file) {
        try {
            return Files.readString(file.toPath());
        } catch (IOException e) {
            System.out.println("Cannot open specified file: " + file.getName());
            return null;
        }
    }

    private static String docReader(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            XWPFDocument document = new XWPFDocument(fis);
            List<XWPFParagraph> data = document.getParagraphs();

            StringBuilder str = new StringBuilder();
            for(XWPFParagraph p : data) {
                str.append(" ").append(p.getText());
            }
            return str.toString();

        } catch (Exception e) {
            System.out.println("Cannot read specified file: " + file.getName());
            return null;
        }
    }
}
